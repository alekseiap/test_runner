#!/usr/bin/env bash


FILES=("1" "2" "3" "4" "5" "6" "7" "8" "9")



for file in "${FILES[@]}"; do {
  git checkout main
  git pull
  echo $RANDOM >> "${file}.txt"
  git add "${file}.txt"
  git commit -m "new file added"
  git push --force

}


echo "script finished"
done